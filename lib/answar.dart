import 'package:flutter/material.dart';

class Answar extends StatelessWidget {
  final VoidCallback selectHandler;
  final String answer;

  Answar(this.answer, this.selectHandler);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(10),
      child: RaisedButton(
        color: Colors.blue,
        child: Text(answer),
        onPressed: selectHandler,
      ),
    );
  }
}
