import 'package:flutter/material.dart';
import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppSate();
  }
}

class MyAppSate extends State<MyApp> {
  var _questionsIndex = 0;
  var _totalScore = 0;
  List _questions = [
    {
      "questiontext": "what's your favourite color ",
      "answers": [
        {
          "text": "White",
          "score": 1
        },
        {
          "text": "Blak",
          "score": 10
        },
        {
          "text": "Red",
          "score": 5
        },
        {
          "text": "Green",
          "score": 3
        },
      ]
    },
    {
      "questiontext": "what's your favourite animal",
      "answers": [
        {
          "text": "Lion",
          "score": 1
        },
        {
          "text": "Tiger",
          "score": 2
        },
        {
          "text": "Lepord",
          "score": 3
        },
        {
          "text": "Elephant",
          "score": 4
        },
      ]
    },
    {
      "questiontext": "what's your favourite food",
      "answers": [
        {
          "text": "Biryani",
          "score": 1
        },
        {
          "text": "Shawarma",
          "score": 2
        },
        {
          "text": "Kheema",
          "score": 3
        },
        {
          "text": "Rice",
          "score": 4
        },
      ]
    }
  ];
  void _answerQuestion(int score) {
    if (_questionsIndex < _questions.length) {
      _totalScore += score;
      setState(() {
        _questionsIndex = _questionsIndex + 1;
      });
      print(_questionsIndex);
    }
  }

  void _restartApp() {
    setState(() {
      _questionsIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),
        ),
        body: _questionsIndex < _questions.length
            ? Quiz(
                selectHandler: _answerQuestion,
                questionsIndex: _questionsIndex,
                questions: _questions,
              )
            : Result(_totalScore, _restartApp),
      ),
    );
  }
}
