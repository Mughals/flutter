import 'package:flutter/material.dart';
import './Question.dart';
import './answar.dart';

class Quiz extends StatelessWidget {
  var questions;
  final int questionsIndex;
  final Function selectHandler;

  Quiz({
    @required this.questions,
    @required this.selectHandler,
    @required this.questionsIndex,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(
          questions[questionsIndex]["questiontext"] as String,
        ),
        ...(questions[questionsIndex]["answers"] as List<Map<String, Object>>).map((answer) {
          return Answar(answer["text"] as String, () => selectHandler(answer["score"]));
        }).toList()
      ],
    );
  }
}
