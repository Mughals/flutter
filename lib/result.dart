import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final VoidCallback restaetHandler;
  Result(this.resultScore, this.restaetHandler);

  String get resultPhase {
    var resultText;
    if (resultScore <= 8) {
      resultText = "You Did It!";
    } else if (resultScore <= 12) {
      resultText = "You Awesome and Innocent";
    } else if (resultScore <= 16) {
      resultText = "pretty likeable";
    } else if (resultScore <= 20) {
      resultText = "You Awesome and Innocent";
    } else {
      resultText = "you so bad";
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Text(
        resultPhase,
        style: TextStyle(
          fontSize: 36,
          fontWeight: FontWeight.bold,
        ),
        textAlign: TextAlign.center,
      ),
      FlatButton(
        child: Text(
          'Restart Quiz',
          style: TextStyle(
            fontSize: 10,
            color: Colors.green,
          ),
        ),
        onPressed: restaetHandler,
        color: Colors.green,
      ),
    ]);
  }
}
